#!/usr/bin/env python
# coding: utf-8


import pandas as pd
import numpy as np
import os
import sys
import io

import tensorflow as tf
from transformers import (AutoTokenizer, TFAutoModel,  AutoModel, pipeline, TFAutoModelForSequenceClassification, TFAutoModelForSeq2SeqLM)

from sentence_transformers import SentenceTransformer, util
import itertools


import warnings
warnings.filterwarnings('ignore')


sys.path.append('../scripts/')
from read_text_from_file import *
from zero_shot_learning_transformers_modified import (summarize_text, calculate_prob_labels)


# In[7]:


#datasets = "/datasets/"
#path_datasets = "..{0}".format(datasets)

PATH_MODEL_CLASSIFIER = "../model/classifier/"
PATH_MODEL_SUMMARIZER = "../model/summarizer/"
PATH_SENTENCE_MODEL = "../model/sentence_transformer/"


# In[8]:


#ec_business_capability_model = pd.read_csv(path_datasets + 'EC business capability model_corr.csv')
#tccc_business_capability_model = pd.read_csv(path_datasets + 'TCCC_capability_model.csv')
#construction_capability_model = pd.read_csv(path_datasets + 'construction_capabilities.csv')



#TracesNT_file = "../data/docx_files/Traces NT Summary.docx"

#text = remove_unneeded_chars_file(check_file_extension(TracesNT_file))



summary_model = TFAutoModelForSeq2SeqLM.from_pretrained(PATH_MODEL_SUMMARIZER)
summary_tokenizer = AutoTokenizer.from_pretrained(PATH_MODEL_SUMMARIZER)



zsl_model = TFAutoModelForSequenceClassification.from_pretrained(PATH_MODEL_CLASSIFIER)
zsl_tokenizer = AutoTokenizer.from_pretrained(PATH_MODEL_CLASSIFIER)


# In[12]:


similarity_model = SentenceTransformer(model_name_or_path=PATH_SENTENCE_MODEL)


# ### Deriving capabilities using keywords extraction

# In[13]:


import nltk
from nltk.stem.wordnet import WordNetLemmatizer


# In[14]:


nltk.download('averaged_perceptron_tagger')
nltk.download('wordnet')


# In[15]:


def modify_tuples(tuples):
    """
        Modify a list of tuples in the form:  [('Ability', 'NN'), ('to', 'TO'), ('manage', 'VB'), ('centralised', 'JJ')],
        under a certain condition concerning VBZ - verb, present tense, 3rd person singular, into 
        verb, present tense, 1st person singular.
    
    """
    lemmatizer = WordNetLemmatizer()
    
    result = []
    
    for key, value in tuples:
        buffer = []
        if value == "VBZ":
            # do processing
            transformed_key = lemmatizer.lemmatize(key.lower(), 'v')
            buffer.append(transformed_key)
            buffer.append(value)
        else:
            buffer.append(key)
            buffer.append(value)
            
        result.append(tuple(buffer))
        
    return result


# In[16]:


def derive_keywords(description):
    '''
        Derive keywords based on regular expressions on Part of Speech tagging. 
    '''
    
    chunk_gram = r"""chunk: {<VB|VBG|NN|JJ><JJ|IN>?<JJ>?<NN|NNS|NNP|NNPS>+}"""
    
    
    words = nltk.word_tokenize(description)
    tagged_words = nltk.pos_tag(words) # tag each word with corresponding Part of Speech
    modified_tagged_words = modify_tuples(tagged_words)
    
    chunk_parser = nltk.RegexpParser(chunk_gram)
    chunked_text = chunk_parser.parse(modified_tagged_words)
      
    keywords = []
    
    for subtree in chunked_text.subtrees(filter=lambda t: t.label() == 'chunk'):
        chunked_output = ' '.join([w for w, t in subtree.leaves()])
        keywords.append(chunked_output[0].lower() + chunked_output[1:])
        
    return keywords


# In[20]:


def derive_weighted_keywords(description, capability, similarity_model):
    """
        Derive keyphrases using weights calculated between each keyphrase and the corresponding capability. In addition,
        a threshold of 0.4 is incorporated, i.e. the keyphrase list includes items with scores above 0.4.
        0.4 is derived empirically.
    """

    keywords_list = derive_keywords(description)
    capability_embedding = similarity_model.encode(capability, convert_to_tensor=True)
    keyword_embedding = similarity_model.encode(keywords_list, convert_to_tensor=True)
    
    cos_scores = util.cos_sim(capability_embedding, keyword_embedding).cpu().numpy()[0]
    weighted_keywords = {keywords_list[i]: cos_scores[i] for i in range(len(keywords_list)) if cos_scores[i] > 0.4}
    
    return weighted_keywords


# In[21]:


def keywords_weights_automation(capability_file, similarity_model):
    '''
        Return a list of keywords with weights above 0.4 for each description in the capability file.
    '''
    wighted_keywords = [derive_weighted_keywords(x, y, similarity_model) for (x, y) in capability_file[['Description', ' LVL 2']].itertuples(index=False)]
    capability_file['weighted_keywords'] = wighted_keywords
    
    
    return capability_file


# In[22]:


capability_model_weighted = keywords_weights_automation(ec_business_capability_model, similarity_model)


# In[37]:


#capability_model_weighted.head(15)


# In[24]:


def all_weighted_keywords(capability_file, similarity_model):
    '''
        Return a list of all weighted keywords comprising all description in the capability file.
    '''
    
    keywords_list = [derive_weighted_keywords(x, y, similarity_model) for (x, y) in capability_file[['Description', ' LVL 2']].itertuples(index=False)]
    all_keywords_list = [item for keywords_sublist in keywords_list for item in keywords_sublist]
    
    return all_keywords_list


# In[25]:


all_weighted_kwds = all_weighted_keywords(ec_business_capability_model, similarity_model)


# In[26]:


#len(all_weighted_kwds)


# In[27]:


def derive_most_relevant_weighted_keywords(zsl_model, zsl_tokenizer, weighted_keywords_list, text, threshold=0.5):
    """
        Derive the most relevant weighted keywords based on the corresponding capability description.
    
    """
    
    most_relevant_keywords = calculate_prob_labels(zsl_model, zsl_tokenizer, text, weighted_keywords_list, threshold)
    
    return most_relevant_keywords


# In[28]:


def select_weighted_capabilities_by_keywords(capability_file, weighted_keywords_list, text):
    """
        Select those weighted capabilities that correspond to the most relevant keywords by index.
    """
    
    most_relevant_keywords = derive_most_relevant_weighted_keywords(zsl_model, zsl_tokenizer, weighted_keywords_list, text, threshold=0.5)
    
    capability_file["value"] = capability_file['weighted_keywords'].map(lambda x: " ".join(list(set(x).intersection(set(most_relevant_keywords.keys())))))
    df_most_relevant_keywords = pd.DataFrame.from_dict(most_relevant_keywords, 'index').reset_index()
    
    df_merged = df_most_relevant_keywords.merge(capability_file, how='inner',
                                                left_on='index', right_on='value')

    relevant_capabilities = df_merged.rename(columns = {' LVL 2': 'Capability', 0: 'Confidence'}, 
                                             inplace = False)[['Capability', 'Confidence']]
    
    return relevant_capabilities.set_index('Capability').T.to_dict('records')[0]


# In[222]:


print(select_weighted_capabilities_by_keywords(ec_business_capability_model, all_weighted_kwds, text))





