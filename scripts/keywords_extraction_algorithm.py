#!/usr/bin/env python
# coding: utf-8

# In[7]:


import pandas as pd
import numpy as np
import os
import sys
import io
import tensorflow as tf
from transformers import (AutoTokenizer, TFAutoModel,  AutoModel, pipeline, TFAutoModelForSequenceClassification, TFAutoModelForSeq2SeqLM)

import itertools

#import warnings
#warnings.filterwarnings('ignore')


# In[8]:


# Format scientific notations
#pd.set_option('display.float_format', lambda x: '%.3f' % x)


# In[9]:


sys.path.append('../scripts/')
from read_text_from_file import *
from zero_shot_learning_transformers_modified import summarize_text, calculate_prob_labels


# In[10]:


datasets = "/datasets/"
path_datasets = "..{0}".format(datasets)

PATH_MODEL_CLASSIFIER = "../model/classifier/"
PATH_MODEL_SUMMARIZER = "../model/summarizer/"


# In[11]:


#ec_business_capability_model = pd.read_csv(path_datasets + 'EC business capability model_corr.csv')
#tccc_business_capability_model = pd.read_csv(path_datasets + 'TCCC_capability_model.csv')


# In[12]:


#TracesNT_file = "../data/docx_files/Traces NT Summary.docx"
#text = remove_unneeded_chars_file(check_file_extension(TracesNT_file))



# In[13]:


summary_model = TFAutoModelForSeq2SeqLM.from_pretrained(PATH_MODEL_SUMMARIZER)
summary_tokenizer = AutoTokenizer.from_pretrained(PATH_MODEL_SUMMARIZER)


# In[31]:


zsl_model = TFAutoModelForSequenceClassification.from_pretrained(PATH_MODEL_CLASSIFIER)
zsl_tokenizer = AutoTokenizer.from_pretrained(PATH_MODEL_CLASSIFIER)


# ### Deriving capabilities using keywords extraction

# In[18]:


import nltk
from nltk.stem.wordnet import WordNetLemmatizer


# In[19]:


nltk.download('averaged_perceptron_tagger')
nltk.download('wordnet')


# In[20]:


def modify_tuples(tuples):
    """
        Modify a list of tuples in the form:  [('Ability', 'NN'), ('to', 'TO'), ('manage', 'VB'), ('centralised', 'JJ')],
        under a certain condition concerning VBZ - verb, present tense, 3rd person singular, into 
        verb, present tense, 1st person singular.
    
    """
    lemmatizer = WordNetLemmatizer()
    
    result = []
    
    for key, value in tuples:
        buffer = []
        if value == "VBZ":
            # do processing
            transformed_key = lemmatizer.lemmatize(key.lower(), 'v')
            buffer.append(transformed_key)
            buffer.append(value)
        else:
            buffer.append(key)
            buffer.append(value)
            
        result.append(tuple(buffer))
        
    return result


# In[21]:


def derive_keywords(description):
    '''
        Derive keywords based on regular expressions on Part of Speech tagging. 
    '''
    
    chunk_gram = r"""chunk: {<VB|VBG|NN><JJ>?<NN|NNS|NNP|NNPS>+}"""
    
    
    words = nltk.word_tokenize(description)
    tagged_words = nltk.pos_tag(words) # tag each word with corresponding Part of Speech
    modified_tagged_words = modify_tuples(tagged_words)
    
    chunk_parser = nltk.RegexpParser(chunk_gram)
    chunked_text = chunk_parser.parse(modified_tagged_words)
      
    keywords = []
    
    for subtree in chunked_text.subtrees(filter=lambda t: t.label() == 'chunk'):
        chunked_output = ' '.join([w for w, t in subtree.leaves()])
        keywords.append(chunked_output[0].lower() + chunked_output[1:])
        
    return keywords


# In[22]:


def keywords_automation(capability_file):
    '''
        Return a list of keywords for each description in the capability file.
    '''
    keywords_list = [derive_keywords(x) for x in capability_file['Description']]
    capability_file['Keywords'] = keywords_list
    
    return capability_file


# In[23]:


def all_keywords(capability_file):
    '''
        Return a list of all keywords comprising all description in the capability file.
    '''
    keywords_list = [derive_keywords(x) for x in capability_file['Description']]
    all_keywords_list = [item for keywords_sublist in keywords_list for item in keywords_sublist]
    
    return all_keywords_list


# In[28]:


def derive_most_relevant_keywords(zsl_model, zsl_tokenizer, capability_file, text, threshold=0.5):
    """
        Derive the most relevant keywords based on the corresponding capability description.
    
    """
    
    capability_file_with_keywords = keywords_automation(capability_file)
    keywords_list = all_keywords(capability_file_with_keywords)
    
    most_relevant_keywords = calculate_prob_labels(zsl_model, zsl_tokenizer, text, keywords_list, threshold)
    
    return most_relevant_keywords


# In[29]:


def select_capabilities_by_keywords(capability_file, text):
    """
        Select those capabilities that corresponds to the most relevant keywords by index.
    """
    
    most_relevant_keywords = derive_most_relevant_keywords(zsl_model, zsl_tokenizer, capability_file, text, threshold=0.5)
    
    capability_file["value"] = capability_file['Keywords'].map(lambda x: " ".join(list(set(x).intersection(set(most_relevant_keywords.keys())))))
    df_most_relevant_keywords = pd.DataFrame.from_dict(most_relevant_keywords, 'index').reset_index()
    
    df_merged = df_most_relevant_keywords.merge(ec_business_capability_model, how='inner',
                                                left_on='index', right_on='value')

    relevant_capabilities = df_merged.rename(columns = {' LVL 2': 'Capability', 0: 'Confidence'}, 
                                             inplace = False)[['Capability', 'Confidence']]
    
    return relevant_capabilities.set_index('Capability').T.to_dict('records')[0]


# In[30]:


print(select_capabilities_by_keywords(ec_business_capability_model, text))

if __name__ == '__main__':

    modify_tuples()
    derive_keywords()
    all_keywords
