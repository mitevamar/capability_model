#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import os
import sys
import io
import warnings
from collections import Counter

import tensorflow as tf

from transformers import (AutoTokenizer, TFAutoModel, pipeline, TFT5ForConditionalGeneration, T5Tokenizer, 
                          TFAutoModelWithLMHead, TFAutoModelForSequenceClassification, 
                          TFAutoModelForSeq2SeqLM, ZeroShotClassificationPipeline)


# In[2]:


sys.path.append('../scripts/')
from read_text_from_file import *


# In[8]:


datasets = "/datasets/"
path_datasets = "..{0}".format(datasets)

PATH_MODEL_SUMMARIZER = "../model/summarizer/"
PATH_MODEL_CLASSIFIER = "../model/classifier/"


# In[9]:


df_ec_business = pd.read_csv(path_datasets + 'EC_Business_Capability.csv')


# In[10]:


ec_business_capabilities = list(set(df_ec_business['Name']))


# In[11]:


test_file = "../data/docx_files/DSMP input OIB_mobile devices.docx"


# In[12]:


text = remove_unneeded_chars_file(check_file_extension(test_file))


# ### Summarizer

# In[13]:


summary_model = TFAutoModelForSeq2SeqLM.from_pretrained(PATH_MODEL_SUMMARIZER)
summary_tokenizer = AutoTokenizer.from_pretrained(PATH_MODEL_SUMMARIZER)


# In[14]:


def summarize_text(summary_model, summary_tokenizer, sequences):
    '''
        Abstractive text summarization.       
    '''
    
    inputs = summary_tokenizer.encode("summarize: " + sequences, return_tensors="tf")
    outputs = summary_model.generate(inputs,
                             length_penalty=2.0,
                             max_length=400, 
                             min_length=100, 
                             num_beams=4, 
                             do_sample=True,
                             no_repeat_ngram_size=3,
                             early_stopping=True)

    summary = summary_tokenizer.decode(outputs[0])
    
    return list({summary.replace('<pad>', '').replace('</s>', '').strip() for x in summary})


# ### Classifier

# In[16]:


zsl_model = TFAutoModelForSequenceClassification.from_pretrained(PATH_MODEL_CLASSIFIER)
zsl_tokenizer = AutoTokenizer.from_pretrained(PATH_MODEL_CLASSIFIER)


# In[17]:


def calculate_prob_labels(zsl_model, zsl_tokenizer, sequences, candidate_labels, threshold=0.5):
    '''
        It returns a dictionary containing capabilities and the respective confidences relevant for a certain document.       
    '''
    
    template = "This example is {}."
    
    sequence = summarize_text(summary_model, summary_tokenizer, sequences.lower())
    
    inputs = zsl_tokenizer(sequence*len(candidate_labels), [template.format(label) for label in candidate_labels], 
                           return_tensors="tf", padding=True)
    
    outputs = zsl_model(inputs)[0]
    
    entail_contradiction_logits = outputs.numpy()[:, [0,2]]
    probs = tf.nn.softmax(entail_contradiction_logits)
    prob_label_is_true = probs[:,1]
    
    conf = [(candidate_labels[i], prob) for i, prob in enumerate(prob_label_is_true.numpy()) if prob > threshold]
    
    return dict(sorted(conf, key=lambda kv: kv[1], reverse=True))


# In[18]:


print(calculate_prob_labels(zsl_model, zsl_tokenizer, text, ec_business_capabilities, threshold=0.5)) # text and ec_business_capabilities


# In[ ]:




