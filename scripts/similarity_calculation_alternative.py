#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import sys
import ast

from transformers import AutoTokenizer, AutoModel
import torch
import torch.nn.functional as F

from sentence_transformers import SentenceTransformer, util


# In[3]:


#torch.cuda.device_count()


# In[18]:


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


# In[19]:


device


# In[4]:


sys.path.insert(0, '../scripts/')


# In[5]:


from read_text_from_file import *


# In[7]:


datasets = "/datasets/"
path_datasets = "..{0}".format(datasets)

PATH_SENTENCE_MODEL = "../model/sentence_transformer/"


# In[23]:


tokenizer = AutoTokenizer.from_pretrained(PATH_SENTENCE_MODEL)
model = AutoModel.from_pretrained(PATH_SENTENCE_MODEL).to(device)


# In[24]:


model.device


# In[25]:


datasets = "/datasets/"
path_datasets = "..{0}".format(datasets)

PATH_SENTENCE_MODEL = "../model/sentence_transformer/"


# In[26]:


ec_business_capability_model = pd.read_csv(path_datasets + 'EC business capability model_corr.csv')
tccc_business_capability_model = pd.read_csv(path_datasets + 'TCCC_capability_model.csv')


# In[12]:


ec_business_capability_model.head()


# In[13]:


TracesNT_file = "../data/docx_files/Traces NT Summary.docx"
LaRa_file = "../data/docx_files/LaRA-FS-part1-v2.0.docx"
COS_file = "../data/docx_files/COS_vision_and_scope.docx"
test_file3 = "../data/pdf_files/Image+Database_Technical+Specifications.pdf"
Cluster_Open_data_file = "../data/pdf_files/Cluster Open Data - MARE.pdf"
Digital_Interviewing_file = "../data/pdf_files/Digital interviewing software.pdf"
DSMP_data_file = "../data/docx_files/DSMP Data for Policy.docx"
Anaplan_file = "../data/pdf_files/TCCC_Anaplan_Platform Overview.pdf"
DSMP_DG_file = "../data/docx_files/DG ENV - DSMP fiches_3.docx"
HRMMS_file = "../data/pdf_files/Development and implementation of HRMMS.pdf"
Informatica_file = "../data/pdf_files/Informatica - Overview Guide.pdf"
SAP_file = "../data/pdf_files/SAP Customer Data Platform.pdf"
VDocument_file = "../data/docx_files/Vision Document_capacity4dev eu_v4.docx"
DOC_file = "../data/docx_files/DOC_1190.docx"

text = remove_unneeded_chars_file(check_file_extension(TracesNT_file))
text2 = remove_unneeded_chars_file(check_file_extension(LaRa_file))
text3 = remove_unneeded_chars_file(check_file_extension(COS_file))
text4 = remove_unneeded_chars_file(check_file_extension(test_file3))
text5 = remove_unneeded_chars_file(check_file_extension(Cluster_Open_data_file))
text6 = remove_unneeded_chars_file(check_file_extension(Digital_Interviewing_file))
text7 = remove_unneeded_chars_file(check_file_extension(DSMP_data_file))
text8 = remove_unneeded_chars_file(check_file_extension(Anaplan_file))
text9 = remove_unneeded_chars_file(check_file_extension(DSMP_DG_file))
text10 = remove_unneeded_chars_file(check_file_extension(HRMMS_file))
text11 = remove_unneeded_chars_file(check_file_extension(Informatica_file))
text12 = remove_unneeded_chars_file(check_file_extension(SAP_file))
text13 = remove_unneeded_chars_file(check_file_extension(VDocument_file))
text14 = remove_unneeded_chars_file(check_file_extension(DOC_file))


# In[14]:


def calibration_function(cosine_similarity):
    """
        This estimator scales and translates a feature such that it is in the range between zero and one.   
    """
    
    min_value = np.intc(0)
    max_value = np.intc(1)
    
    min_value_from_distr = min(cosine_similarity)
    max_value_from_distr = max(cosine_similarity)
    
    X_std = (cosine_similarity - min_value_from_distr) / (max_value_from_distr - min_value_from_distr)
    X_scaled = X_std * (max_value - min_value) + min_value
    
    return X_scaled


# In[15]:


def mean_pooling(model_output, attention_mask):
    
    token_embeddings = model_output[0] #First element of model_output contains all token embeddings
    input_mask_expanded = attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
    
    return torch.sum(token_embeddings * input_mask_expanded, 1) / torch.clamp(input_mask_expanded.sum(1), min=1e-9)


# In[33]:


def cosine_similarity_calculation_alternative(model, tokenizer, text, capability_file):
    
    '''
        It automates the calculation of the cosine similarity between all descriptions and a certain text.
        It uses AutoTokenizer and AutoModel with Roberta model instead of SentenceTransformer.
    '''

    doc_text_string = remove_stop_words_file(remove_unneeded_chars_file(text))
    description_strings = capability_file['Description'].map(lambda x: remove_stop_words_file(remove_unneeded_chars_file(x)))
    
    # Tokenize sentences
    encoded_doc_text = tokenizer(doc_text_string, padding=True, truncation=True, max_length=512, return_tensors='pt')
    encoded_descriptions = tokenizer(description_strings.to_list(), padding=True, truncation=True, max_length=512, return_tensors='pt')
    
    # Compute token embeddings
    with torch.no_grad():
        model_output_doc_text = model(**encoded_doc_text.to(device))
        model_output_descriptions = model(**encoded_descriptions.to(device))

    # Perform pooling
    doc_text_embeddings = mean_pooling(model_output_doc_text, encoded_doc_text['attention_mask'])
    descriptions_embeddings = mean_pooling(model_output_descriptions, encoded_descriptions['attention_mask'])

    # Normalize embeddings
    doc_text_embeddings = F.normalize(doc_text_embeddings, p=2, dim=1)
    descriptions_embeddings = F.normalize(descriptions_embeddings, p=2, dim=1)
    
    cos_scores = util.cos_sim(doc_text_embeddings, descriptions_embeddings).cpu().numpy()[0]
    
    capability_file['cosine_similarity'] = [cos_score for cos_score in cos_scores]
    
    capability_file['cos_score_calibrated'] = calibration_function(capability_file['cosine_similarity'])
    
    results = capability_file[[' LVL 2', 'cos_score_calibrated']].sort_values('cos_score_calibrated', ascending=False).head(10)
    
    return results.set_index(' LVL 2').T.to_dict('records')[0]


# In[34]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text, ec_business_capability_model)')


# In[21]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text2, ec_business_capability_model)')


# In[35]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text2, ec_business_capability_model)')


# In[36]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text3, ec_business_capability_model)')


# In[37]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text4, ec_business_capability_model)')


# In[38]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text5, ec_business_capability_model)')


# In[39]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text6, ec_business_capability_model)')


# In[40]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text7, ec_business_capability_model)')


# In[41]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text8, ec_business_capability_model)')


# In[42]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text9, ec_business_capability_model)')


# In[43]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text10, ec_business_capability_model)')


# In[44]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text11, tccc_business_capability_model)')


# In[45]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text12, tccc_business_capability_model)')


# In[46]:


get_ipython().run_cell_magic('time', '', 'cosine_similarity_calculation_alternative(model, tokenizer, text13, ec_business_capability_model)')

