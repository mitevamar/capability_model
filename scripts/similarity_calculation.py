#!/usr/bin/env python
# coding: utf-8

# In[78]:


import pandas as pd
import numpy as np
import sys

#from sklearn.preprocessing import quantile_transform

from sentence_transformers import SentenceTransformer, util


# In[99]:


sys.path.append('../scripts/')
from read_text_from_file import *


# In[100]:


datasets = "/datasets/"
path_datasets = "..{0}".format(datasets)

PATH_SENTENCE_MODEL = "../model/sentence_transformer/"

#NUM_CAPABILITIES = 78 #78 corrsponds to the number of capabilities in all

#NUM_CAPABILITIES = len(ec_business_capability_model[" LVL 2"])


# In[101]:


ec_business_capability_model = pd.read_csv(path_datasets + 'EC business capability model_corr.csv')


# In[102]:


#ec_business_capability_model.head()


# In[26]:


#TracesNT_file = "../data/docx_files/Traces NT Summary.docx"
#text = remove_unneeded_chars_file(check_file_extension(TracesNT_file))


# In[22]:


similarity_model = SentenceTransformer(model_name_or_path=PATH_SENTENCE_MODEL)


# In[96]:

def calibration_function(cosine_similarity):
    """
        This estimator scales and translates a feature such that it is in the range between zero and one.   
    """
    
    min_value = np.intc(0)
    max_value = np.intc(1)
    
    min_value_from_distr = min(cosine_similarity)
    max_value_from_distr  = max(cosine_similarity)
    
    X_std = (cosine_similarity - min_value_from_distr) / (max_value_from_distr - min_value_from_distr)
    X_scaled = X_std * (max_value - min_value) + min_value
    
    return X_scaled


def cosine_similarity_calculation(model, text, capability_file):
    '''
        It automates the calculation of the cosine similarity between all descriptions and a certain text.
    '''
    
    summary_string = remove_stop_words_file(remove_unneeded_chars_file(text))
    description_strings = capability_file['Description'].map(lambda x: remove_stop_words_file(remove_unneeded_chars_file(x)))
    
    summary_embedding = model.encode(summary_string, convert_to_tensor=True)
    description_embeddings = model.encode(description_strings, convert_to_tensor=True)
    
    cos_scores = util.cos_sim(summary_embedding, description_embeddings).cpu().numpy()[0]
    
    capability_file['cosine_similarity'] = [cos_score for cos_score in cos_scores]
    
    capability_file['cos_score_calibrated'] = calibration_function(capability_file['cosine_similarity'])
    
    results = capability_file[[' LVL 2', 'cos_score_calibrated']].sort_values('cos_score_calibrated', ascending=False).head(10)
    
    return results.set_index(' LVL 2').T.to_dict('records')[0]


# In[97]:


print(cosine_similarity_calculation(similarity_model, text, ec_business_capability_model))

